/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Christian Mandery
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*/

#include <stdexcept>

#include "UnlabeledMarkerMotion.h"

using namespace MMM;

unsigned int UnlabeledMarkerMotion::getNumFrames()
{
    return (unsigned int)markerTrajectories.size();
}

std::string UnlabeledMarkerMotion::toXML()
{
    return "";  // TODO: Not implemented
}

void UnlabeledMarkerMotion::appendFrame(UnlabeledMarkerDataPtr data)
{
    if (!data)
        throw std::runtime_error("Empty data!");

    markerTrajectories.push_back(data);
}

UnlabeledMarkerDataPtr UnlabeledMarkerMotion::getFrame(size_t frame) const
{
    if (frame >= markerTrajectories.size())
        throw std::overflow_error("Invalid frame number!");

    return markerTrajectories[frame];
}

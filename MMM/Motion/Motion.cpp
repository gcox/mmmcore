#include "Motion.h"
#include "../rapidxml.hpp"
#include "../XMLTools.h"
#include <boost/filesystem.hpp>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/rolling_mean.hpp>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/LU>

using namespace boost::accumulators;

using std::cout;
using std::endl;


namespace MMM
{

Motion::Motion (const std::string& name)
:name(name)
{
}

Motion::Motion (const Motion& m):
    motionFrames(m.motionFrames),
    jointNames(m.jointNames),
    name(m.name),
    model(m.model),
    originalModel(m.originalModel),
    modelProcessor(m.modelProcessor),
    motionFilePath(m.motionFilePath),
    motionFileName(m.motionFileName)
{
}

MotionPtr Motion::copy(){
    MotionPtr m(new Motion("copy"));
    m->motionFrames.clear();
    for(size_t i = 0; i < this->motionFrames.size(); i++){
        MotionFramePtr mfp = this->motionFrames[i]->copy();
        m->motionFrames.push_back(mfp);
    }

    m->jointNames = this->jointNames;
    m->name = this->name;
    m->model = this->model;
    m->originalModel = this->originalModel;
    m->modelProcessor = this->modelProcessor;

    return m;
}

bool Motion::addMotionFrame(MotionFramePtr md)
{
	if (!md)
		return false;
	if (jointNames.size()>0 && md->joint.rows()>0)
		if (md->joint.rows() != jointNames.size())
		{
			MMM_ERROR << "Error: md.joint.rows()=" << md->joint.rows() << ", but jointNames.size()=" << jointNames.size() << endl;
			return false;
		}
	motionFrames.push_back(md);
    return true;
}

bool Motion::removeMotionFrame(size_t frame)
{
    if(frame >= motionFrames.size())
        return false;
    motionFrames.erase(motionFrames.begin()+frame);
    return true;
}

bool Motion::setJointOrder(const std::vector<std::string> &jointNames)
{
	if (jointNames.size()==0)
		return false;
	this->jointNames = jointNames;
	return true;
}

void Motion::setComment(const std::string &comment)
{
	CommentEntryPtr c(new CommentEntry());
	c->comments.push_back(comment);
	addEntry("comments",c);
}


void Motion::addComment(const std::string &comment)
{
    if (!hasEntry("comments"))
        setComment(comment);
    else
    {
        MotionEntryPtr c1 = getEntry("comments");
        CommentEntryPtr c = boost::dynamic_pointer_cast<CommentEntry>(c1);
        if (c)
            c->comments.push_back(comment);
    }
}

void Motion::setModel(ModelPtr model)
{
	this->model = model;
	this->originalModel = model;
}

void Motion::setModel(ModelPtr processedModel, ModelPtr originalModel)
{
	this->model = processedModel;
    this->originalModel = originalModel;
}

ModelPtr Motion::getModel(bool processedModel)
{
    if(processedModel)
        return model;
    else
        return originalModel;
}

const std::string &Motion::getMotionFilePath()
{
    return motionFilePath;
}

const std::string &Motion::getMotionFileName()
{
    return motionFileName;
}

void Motion::setMotionFilePath(const std::string &filepath)
{
    this->motionFilePath = filepath;
}

void Motion::setMotionFileName(const std::string &filename)
{
    this->motionFileName = filename;
}

void Motion::setModelProcessor(ModelProcessorPtr mp)
{
    this->modelProcessor = mp;
}

ModelProcessorPtr Motion::getModelProcessor()
{
    return modelProcessor;
}

std::string Motion::toXML()
{
	std::string tab1 = "\t";
	std::string tab2 = "\t\t";
	std::string tab3 = "\t\t\t";
	std::stringstream res;
	//res << "<? xml version='1.0' ?>" << endl;

    res << tab1 << "<Motion name='" << name << "'>" << endl;
	std::map<std::string, MotionEntryPtr>::iterator i = motionEntries.begin();
	while (i != motionEntries.end())
	{
		res << i->second->toXML();
		i++;
	}
	if (originalModel && !originalModel->getFilename().empty())
	{
		std::string modelFile = originalModel->getFilename();
		if (!motionFilePath.empty())
		{
			// make relative path
            MMM::XML::makeRelativePath(motionFilePath, modelFile);
			//modelFile = MMM::XML::make_relative(motionFilePath, modelFile);
		}

		res << tab2 << "<Model>" << endl;
		res << tab3 << "<File>" << modelFile << "</File>" << endl;
		res << tab2 << "</Model>" << endl;
	}
	if (modelProcessor)
	{
		res << modelProcessor->toXML(2);
	}

	if (jointNames.size()>0)
	{
		res << tab2 << "<JointOrder>" << endl;
		for (size_t i=0;i<jointNames.size();i++)
		{
			res << tab3 << "<Joint name='" << jointNames[i] << "'/>" << endl;
		}
		res << tab2 << "</JointOrder>" << endl;
	}
	res << tab2 << "<MotionFrames>" << endl;		
	for (size_t i=0;i<motionFrames.size();i++)
	{
		res << motionFrames[i]->toXML(); 
	}
	res << tab2 << "</MotionFrames>" << endl;
	res << tab1 << "</Motion>" << endl;
	return res.str();
}

void  Motion::print()
{
	std::string s = toXML();
	cout << s;
}

void Motion::print(const std::string &filename){
    std::ofstream ofs(filename.c_str());
    std::string s = toXML();
    ofs << "<?xml version='1.0'?>\n";
    ofs << "<MMM>\n";
    ofs << s;
    ofs << "</MMM>\n";
    ofs.close();
}

void Motion::outputData(const std::string &filename)
{
    std::ofstream ofs(filename.c_str());

    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr frame = getMotionFrame(i);
        Eigen::Vector3f pos = frame->getRootPos();

        for(int j= 0; j < pos.rows(); j++){
            ofs << pos[j];
            ofs << '\t';
        }

        ofs << '\n';

    }
    ofs.close();
}


std::vector<MotionFramePtr> Motion::getMotionFrames()
{
	return motionFrames;
}

MotionFramePtr Motion::getMotionFrame(size_t frame)
{
	if (frame>=motionFrames.size())
	{
		MMM_ERROR << "OutOfBounds error in getMotionFrame:" << frame << endl;
		return MotionFramePtr();
	}
	return motionFrames[frame];
}

std::vector<std::string> Motion::getJointNames()
{
	return jointNames;
}

unsigned int Motion::getNumFrames()
{
	return (unsigned int)motionFrames.size();
}

std::string Motion::getComment() 
{
	if (!hasEntry("comments"))
		return std::string();
	MotionEntryPtr e = getEntry("comments");
	CommentEntryPtr r = boost::dynamic_pointer_cast<CommentEntry>(e);
	if (!r)
		return std::string();
	return r->getCommentString();
}

void Motion::setName(const std::string& name) {
    this->name = name;
}

std::string Motion::getName() {
    return name;
}


bool Motion::hasJoint(const std::string& name)
{
	std::string lc = name;
	XML::toLowerCase(lc);
	for(int i=0; i<(int)jointNames.size(); i++)
	{
		if(jointNames[i]==name)
		{
			return true;
		}
	}
	return false;
}

void Motion::calculateVelocities(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointValues = getJointValuesAsMatrix();
    Eigen::MatrixXf jointVelocities = calculateDifferentialQuotient(jointValues,method);

    for (size_t i=0; i < frames.size(); i++)
    {
        frames[i]->joint_vel = jointVelocities.row(i);
    }

    return;
}

void Motion::calculateAccelerations(int method)
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    Eigen::MatrixXf jointVelocities = getJointVelocitiesAsMatrix();
    Eigen::MatrixXf jointAcclerations = calculateDifferentialQuotient(jointVelocities,method);

    for (size_t i=0; i < frames.size(); i++)
    {
        frames[i]->joint_acc = jointAcclerations.row(i);
    }

    return;
}

void Motion::smoothJointValues(jointEnum type, int windowSize)
{
    if (type == Motion::eValues)
    {
        Eigen::MatrixXf input = getJointValuesAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint(i) = smoothed_cv(j);
            }

        }

    }
    else if (type == Motion::eVelocities)
    {
        Eigen::MatrixXf input = getJointVelocitiesAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint_vel(i) = smoothed_cv(j);
            }

        }
    }
    else if (type == Motion::eAccelerations)
    {
        Eigen::MatrixXf input = getJointAccelerationsAsMatrix();

        for (int i=0; i < input.cols(); i++)
        {
            Eigen::VectorXf cv = input.col(i);
            Eigen::VectorXf smoothed_cv = applyRollingMeanSmoothing(cv, windowSize);

            // now, write back smoothed values
            for(size_t j=0; j < motionFrames.size(); j++)
            {
                motionFrames[j]->joint_acc(i) = smoothed_cv(j);
            }

        }
    }
}



Eigen::MatrixXf Motion::getJointValuesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointValues = Eigen::MatrixXf::Zero(nFrames, nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointValues.row(i) = frames[i]->joint;
    }

    return jointValues;
}


Eigen::MatrixXf Motion::getJointVelocitiesAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointVelocities = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointVelocities.row(i) = frames[i]->joint_vel;
    }

    return jointVelocities;
}
Eigen::MatrixXf Motion::getJointAccelerationsAsMatrix()
{
    std::vector<MotionFramePtr> frames = getMotionFrames();
    int nFrames = frames.size();
    int nDof = frames[0]->joint.rows();

    Eigen::MatrixXf jointAccelerations = Eigen::MatrixXf::Zero(nFrames,nDof);

    for (int i=0; i < nFrames; i++)
    {
        jointAccelerations.row(i) = frames[i]->joint_acc;
    }

    return jointAccelerations;
}






Eigen::MatrixXf Motion::calculateDifferentialQuotient(const Eigen::MatrixXf &inputMatrix, int method)
{
    Eigen::MatrixXf outputMatrix = Eigen::MatrixXf(inputMatrix.rows(),inputMatrix.cols());

    if (method == 0) // centralized
    {
        Eigen::VectorXf x1 = Eigen::VectorXf(inputMatrix.cols());
        Eigen::VectorXf x2 = Eigen::VectorXf(inputMatrix.cols());


        //iterate over all frames
        for (int i=0; i< inputMatrix.rows(); i++)
        {
            if (i==0)
                x1 = inputMatrix.row(0);
            else
                x1 = inputMatrix.row(i-1);

            if(i==inputMatrix.rows()-1)
                x2 = inputMatrix.row(inputMatrix.rows()-1);
            else
                x2 = inputMatrix.row(i+1);

            outputMatrix.row(i) = (x2-x1)/float(2.0*0.01);
        }

    }

    return outputMatrix;
}

Eigen::VectorXf Motion::applyRollingMeanSmoothing(Eigen::VectorXf &input, int windowSize)
{
    Eigen::VectorXf output = Eigen::VectorXf::Zero(input.rows());
    accumulator_set<float, stats<tag::rolling_mean> > acc(tag::rolling_window::window_size = windowSize);

    for(int i=0; i < input.rows();i++)
    {
        acc(input(i));
        output(i) = rolling_mean(acc);
    }

    return output;
}



MotionPtr Motion::getSegmentMotion(size_t frame1, size_t frame2){
    if(frame1 > frame2){
        size_t tframe = frame1;
        frame1 = frame2;
        frame2 = tframe;
    }

    MotionPtr res(new Motion("na"));
    res = copy();
    std::string cname = getName() + "_segmented";
    res->setName(cname);

    if(frame2 > motionFrames.size()){
        return res;
    }

    std::vector<MotionFramePtr>::iterator begin = motionFrames.begin() + frame1;
    std::vector<MotionFramePtr>::iterator end = motionFrames.begin() + frame2;

    std::vector<MotionFramePtr> newMotionFrames(begin,end);

    res->setMotionFrames(newMotionFrames);

    res->setModel(getModel());
    res->setModelProcessor(getModelProcessor());
    return res;
}


MotionList Motion::getSegmentMotions(std::vector<int> segmentPoints){
    MotionList res;

    std::vector<int>::iterator it = segmentPoints.begin();
    segmentPoints.insert(it, 0);
    segmentPoints.push_back(getNumFrames());
    it = segmentPoints.begin();

    for(size_t i = 0; i < segmentPoints.size()-1; i++){
        std::ostringstream ss;
        ss << i;
        std::string segMotionName = getName() + "_" + "Segment" + ss.str();
        MotionPtr segMotion = getSegmentMotion(segmentPoints[i], segmentPoints[i+1]);
        segMotion->setName(segMotionName);
        segMotion->setModel(getModel());
        segMotion->setModelProcessor(getModelProcessor());
        res.push_back(segMotion);
    }

    return res;
}

void Motion::joinMotion(MotionPtr next)
{
    MotionFramePtr lastFrame = getMotionFrame(getMotionFrames().size()-1);
    float timestep = lastFrame->timestep;

    Eigen::Matrix4f basePose = lastFrame->getRootPose();

    MotionPtr nextCopy = next->copy();
    nextCopy->setStartPose(basePose);

    for(int i = 0; i < nextCopy->getMotionFrames().size(); i++){
        MotionFramePtr frame = nextCopy->getMotionFrame(i)->copy();
        frame->timestep += timestep;
        addMotionFrame(frame);
    }


}

void Motion::setStartPosition(const Eigen::Vector3f &startPosition)
{
    MotionFramePtr startFrame = getMotionFrame(0);
    Eigen::Vector3f basePos = startFrame->getRootPos();
    Eigen::Vector3f diffPos = basePos - startPosition;

    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr curframe = getMotionFrame(i);
        basePos = curframe->getRootPos();
        curframe->setRootPos(basePos - diffPos);
    }
}

void Motion::setStartPose(const Eigen::Matrix4f & startPose)
{
    MotionFramePtr startFrame = getMotionFrame(0);
    Eigen::Matrix4f basePose = startFrame->getRootPose();
    Eigen::Matrix4f diffPose = startPose * basePose.inverse();

    for(size_t i = 0; i < getMotionFrames().size(); i++) {
        MotionFramePtr curframe = getMotionFrame(i);
        basePose = diffPose * curframe->getRootPose();

        curframe->setRootPose(basePose);
    }

}

std::vector<double> Motion::getTimestamps()
{
    std::vector<double> res;
    for(size_t i = 0; i < getMotionFrames().size(); i++){
        MotionFramePtr curframe = getMotionFrame(i);
        res.push_back(curframe->timestep);
    }

    return res;
}

void Motion::setStartTime(double start, double step)
{
    double curTime = start;
    for(size_t i = 0; i < getNumFrames(); i++)
    {
        MotionFramePtr curframe = getMotionFrame(i);
        curframe->timestep = curTime;
        curTime += step;
    }
}



}//namespace



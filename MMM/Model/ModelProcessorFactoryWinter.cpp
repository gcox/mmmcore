
#include "ModelProcessorFactoryWinter.h"
#include "ModelProcessorWinter.h"


namespace MMM
{

// register this factory
ModelProcessorFactory::SubClassRegistry ModelProcessorFactoryWinter::registry(ModelProcessorFactoryWinter::getName(), &ModelProcessorFactoryWinter::createInstance);

ModelProcessorFactoryWinter::ModelProcessorFactoryWinter()
: ModelProcessorFactory()
{

}
ModelProcessorFactoryWinter::~ModelProcessorFactoryWinter()
{

}

ModelProcessorPtr ModelProcessorFactoryWinter::createModelProcessor()
{
    ModelProcessorPtr p(new ModelProcessorWinter());
	return p;
}

std::string ModelProcessorFactoryWinter::getName()
{
	return "Winter";
}

boost::shared_ptr<ModelProcessorFactory> ModelProcessorFactoryWinter::createInstance(void*)
{
	boost::shared_ptr<ModelProcessorFactory> converterFactory(new ModelProcessorFactoryWinter());
	return converterFactory;
}

}

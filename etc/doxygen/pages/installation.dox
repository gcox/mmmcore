/**
\page installation MMM Libraries: Installation

\section installation-intro Introduction
    The MMM Libraries officially support Ubuntu 14.04. The framework has also be tested with Windows 7 with several Visual Studio versions.
	If you encounter any problems with the compilation or installation process, please contact <a href="mailto:mandery@kit.edu?cc=vahrenkamp@kit.edu,asfour@kit.edu">mandery@kit.edu</a>.
    In the following, an exemplary installation setup is given that refers to an Ubuntu 14.04 system.
	For any other operating system or distribution, all dependencies listed must be adapted.

\section installation-prerequisites Prerequisites (Ubuntu)
First of all be sure git and cmake is installed on your system:

\li Git to fetch the project <a href="http://git-scm.com/">Link</a>  and \li <a href="http://cmake.org">CMake 2.8.3</a> for building the project:

\verbatim
    sudo apt-get install git gitk cmake
\endverbatim

\subsection installation-optional Optional packages

\li GUI Packages for git and CMake:

\verbatim
    sudo apt-get install git-gui cmake-qt-gui
\endverbatim

\li <a href="http://www.stack.nl/~dimitri/doxygen/">Doxygen</a> for generating the documentation:

\verbatim
    sudo apt-get install doxygen doxygen-gui
\endverbatim

\section installation-MMMCore MMMCore

The MMMCore Library depends on two libraries:

   - <a href="http://boost.org">Boost (>=1.48)</a> for shared pointers, mutexes, testing, ...
   - <a href="http://eigen.tuxfamily.org/">Eigen 3</a> For algebra

\verbatim
    sudo apt-get install libeigen3-dev libboost-all-dev
\endverbatim


\subsection installation-MMMCore-obtaining Getting MMMCore
The sourcecode of MMMCore can be fetched by issuing the following commands:

\verbatim
    cd ~
    git clone https://gitlab.com/mastermotormap/mmmcore.git MMMCore
\endverbatim

This will create a directory named MMMCore in the home directory of the current user
containing everything which is necessary to get started with MMMCore.


\subsection installation-MMMCore-compilation Compile MMMCore
After all dependencies are installed the compilation of MMMCore can be performed by
executing the following commands:

\verbatim
    cd ~/MMMCore
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim




\section installation-MMMTools MMMTools
    The MMMTools depend on following libraries:

    - <a href="http://qt-project.org/downloads">qt 4.6.3 (or higher, but no qt5)</a> For Gui
    - <a href="https://gitlab.com/Simox/simox">Simox</a> for model loading, coordinate transformations, collision checking, ...
     and previously built \ref installation-MMMCore.

\verbatim
    sudo apt-get install libqt4-*
\endverbatim

For installing SIMOX look here: \ref installation-simox

\subsection installing-NLopt-optional Installing NLopt (optional)

If you want to use the NLopt-based motion converter (very likely, if you want to use MMMConverter/MMMConverterGUI at all!), you need to install the NLopt development package:

\verbatim
    sudo apt-get install libnlopt-dev
\endverbatim

\subsection installation-MMMTools-obtaining Getting MMMTools
The sourcecode of MMMTools can be fetched by issuing the following commands:

\verbatim
    cd ~
    git clone https://gitlab.com/mastermotormap/mmmtools.git MMMTools
\endverbatim

This will create a directory named MMMTools in the home directory of the current user
containing everything which is necessary to get started with MMMTools.


\subsection installation-MMMTools-compilation Compiling MMMTools
After all dependencies are installed the compilation of MMMTools can be performed by
executing the following commands:

\verbatim
    cd ~/MMMTools
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim


Now you can execute ./bin/MMMViewer in your build directory.

\section installation-simox Building Simox

The steps to build Simox are as follows:

\verbatim
    sudo apt-get install libeigen3-dev libcoin60-dev libsoqt4-dev libboost1.48-all-dev libqt4-* doxygen
\endverbatim


\verbatim
    git clone https://gitlab.com/Simox/simox.git
    mkdir simox/build
    cd simox/build
    cmake -DCMAKE_BUILD_TYPE=Release ..
    make
\endverbatim

If cmake is not able to find the depenencies, you either can use cmake-gui/ccmake to specify the locations or you can export some environment variables 
to give some hints to the cmake find scripts. Here, an exemplary setup is shown (<b>usually not needed!</b>):

\verbatim
    export Eigen3_DIR=~/libs/Eigen3/include/eigen3
    export QT_QMAKE_EXECUTABLE=~/libs/Qt-4.7.4/bin/qmake
    export BOOST_ROOT=~/libs/boost/1.47.0
    export SoQt_DIR=~/libs/Coin3D
    export Coin3D_DIR=~/libs/Coin3D
    export Simox_DIR=~/simox
\endverbatim


More detailed steps are descriped here:
 \li <a href="https://gitlab.com/Simox/simox/wikis/Installation">https://gitlab.com/Simox/simox/wikis/Installation</a>

*/
